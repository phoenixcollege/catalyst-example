<?php
require 'autoloader.php';
$config = require 'config.php';
\Catalyst\Logging\Logger::setLevel($config['logging']['level'] ?? \Catalyst\Logging\Logger::INFO);
\Catalyst\Logging\Logger::setHandlers([new \Catalyst\Logging\Handlers\EchoHandler()]);
if ($config['testing'] ?? true) {
    \Catalyst\Db\Test::setConnectCredentials(
        $config['database']['dsn'],
        $config['database']['username'],
        $config['database']['password']
    );
    $dbConnection = \Catalyst\Db\Test::getInstance();
    $dbConnection->addTestData([
        new \Catalyst\Db\TestData\AdhocData([['var_value' => 'x224']]),
        new \Catalyst\Db\TestData\AdhocData([['var_value' => 'license-id-1']]),
        new \Catalyst\Db\TestData\ClassesData(),
        new \Catalyst\Db\TestData\PeopleData(10),
        new \Catalyst\Db\TestData\EnrollmentsData(10),
    ]);
    $dataDir = __DIR__.'/data/';
    $authRequest = new \Catalyst\Request\Test(
        $dataDir,
        [
            new \Catalyst\Request\TestResponse('auth/valid_token.json'),
        ]
    );
    $request = new \Catalyst\Request\Test(
        $dataDir,
        [
            new \Catalyst\Request\TestResponse('myOrg/myOrg.json'),
            new \Catalyst\Request\TestResponse('groups/createGroups.1.many.json'),
            new \Catalyst\Request\TestResponse('groups/createGroups.2.many.json'),
            new \Catalyst\Request\TestResponse('users/getBySsoId.createUser.many.json'),
            new \Catalyst\Request\TestResponse('users/createUser.many.json'),
            new \Catalyst\Request\TestResponse('users/registerUser.many.json'),
            new \Catalyst\Request\TestResponse('users/getBySsoId.enrollments.many.json'),
            new \Catalyst\Request\TestResponse('users/addUserToGroup.many.json'),
            new \Catalyst\Request\TestResponse('users/assignLicense.many.json'),
        ]
    );
} else {
    \Catalyst\Db\Odbc::setConnectCredentials(
        $config['database']['dsn'],
        $config['database']['username'],
        $config['database']['password']
    );
    $dbConnection = \Catalyst\Db\Odbc::getInstance();
    $authRequest = new \Catalyst\Request\Curl($config['catalyst']['authUrl']);
    $request = new \Catalyst\Request\Curl($config['catalyst']['graphqlUrl']);
}

// authentication
$auth = new \Catalyst\Auth\Authenticate($authRequest);
$token = $auth->authenticate($config['catalyst']['username'], $config['catalyst']['password']);

// create the request handler to accept query requestables and return responses
$requestHandler = new \Catalyst\RequestHandler($request, $token);

// create the MyOrg lookup handler
$myOrg = new \Catalyst\Lookups\Catalyst\MyOrg($requestHandler);
// get the org id
$orgId = $myOrg->getId();

// lookup vars
$varsProvider = new \Catalyst\Lookups\App\Vars($dbConnection);
$currentSemester = $varsProvider->get('current_semester');
$licensePoolId = $varsProvider->get($currentSemester.'-LicensePoolID');

// ***** GROUPS *****
\Catalyst\Logging\Logger::write('Entering GROUPS');
// create iterator for classes that are "new" - using chunk size of 10 so it runs the loop twice
$classesProvider = new \Catalyst\Lookups\App\Classes($dbConnection, $orgId);
$newGroupIterator = $classesProvider->fetchNewAsChunk([$currentSemester], 10);

// create catalyst and local updaters
$catalystGroupUpdater = new \Catalyst\Updaters\Catalyst\CreateGroups($requestHandler);
// using a null connection simple logs the query/parameters to debug
$classesUpdater = new \Catalyst\Updaters\App\Classes($dbConnection);
// iterate 'chunks' of new classes, this should loop twice since there are 20 classes and 10 per iterator chunk
foreach ($newGroupIterator as $newGroupChunk) {
    // create groups in catalyst (mass create if possible)
    $catalystGroups = $catalystGroupUpdater->update($newGroupChunk);
    // update the app database with the catalyst models (test in this case)
    $classesUpdater->update($catalystGroups);
}
$errors = $catalystGroupUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'CATALYST_GROUPS:errors');
$errors = $classesUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'LOCAL_CLASSES:errors');

// ***** USERS *****
\Catalyst\Logging\Logger::write('Entering USERS');
// create iterator for users that are 'new' (raw rows since we need to use it for create and register)
$peopleProvider = new \Catalyst\Lookups\App\People($dbConnection, $orgId);
$newPeopleIterator = $peopleProvider->fetchNewAsChunk([], 50, true);

$catalystCreateUserUpdater = new \Catalyst\Updaters\Catalyst\CreateUsers($requestHandler);
$catalystRegisterUserUpdater = new \Catalyst\Updaters\Catalyst\RegisterUsers($requestHandler);
$peopleUpdater = new \Catalyst\Updaters\App\People($dbConnection);
foreach ($newPeopleIterator as $newPeopleChunk) {
    $createUserModels = $peopleProvider->toModels($newPeopleChunk);
    // create users in catalyst (with check for existing by ssoId)
    $catalystUsers = $catalystCreateUserUpdater->update($createUserModels);
    $registerUserModels = $peopleProvider->toModels($catalystUsers, \Catalyst\Model\Input\RegisterUser::class);
    // register users returned from catalyst
    $catalystRegisterUserUpdater->update($registerUserModels);
    // update the app database with the catalyst models
    $peopleUpdater->update($catalystUsers);
}
$errors = $catalystCreateUserUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'CATALYST_CREATE_USERS:errors');
$errors = $catalystRegisterUserUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'CATALYST_REGISTER_USERS:errors');
// reusing the peopleUpdater during licenses (enrollments block) so not outputting errors here

// ***** ENROLLMENTS *****
\Catalyst\Logging\Logger::write('Entering ENROLLMENTS');
// create iterator for enrollments that are 'new' (raw again)
$enrollmentProvider = new \Catalyst\Lookups\App\Enrollments($dbConnection, $orgId, $licensePoolId);
$newEnrollmentIterator = $enrollmentProvider->fetchNewAsChunk([$currentSemester], 50, true);

$usersLookup = new \Catalyst\Lookups\Catalyst\Users($requestHandler);
$addUserToGroupUpdater = new \Catalyst\Updaters\Catalyst\AddUsersToGroups($requestHandler);
$assignLicenseUpdater = new \Catalyst\Updaters\Catalyst\AssignLicenses($requestHandler);
$enrollmentsUpdater = new \Catalyst\Updaters\App\Enrollments($dbConnection);

foreach ($newEnrollmentIterator as $newEnrollmentChunk) {
    // collect array of ssoIds from the chunk of enrollments
    $ssoIds = array_map(function ($value) {
        return $value['ssoid'];
    }, $newEnrollmentChunk);
    // lookup the ssoId from catalyst
    $catalystUsers = $usersLookup->getBySsoIds($ssoIds);
    // add them to the AdddUsersToGroups updater
    $addUserToGroupUpdater->addUsers($catalystUsers);

    $addUserToGroupsModels = $enrollmentProvider->toModels($newEnrollmentChunk);
    $addedModels = $addUserToGroupUpdater->update($addUserToGroupsModels);

    $enrollmentsUpdater->update($addedModels);

    $assignLicenseUpdater->addUsers($addUserToGroupUpdater->getUsers());
    $assignLicensesModels = $enrollmentProvider->toModels($newEnrollmentChunk,
        \Catalyst\Model\Input\AssignLicense::class);
    $assignedLicenses = $assignLicenseUpdater->update($assignLicensesModels);

    $peopleUpdater->updateLicenses($assignedLicenses, $licensePoolId);

    $addUserToGroupUpdater->addUsers($assignLicenseUpdater->getUsers());
}

$errors = $addUserToGroupUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'CATALYST_ADD_USER_TO GROUP:errors');

$errors = $enrollmentsUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'LOCAL_ENROLLMENTS:errors');

$errors = $assignLicenseUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'CATALYST_ASSIGN_LICENSE:errors');

$errors = $peopleUpdater->getErrors();
\Catalyst\Logging\Logger::write($errors, 'LOCAL_PEOPLE:errors');
