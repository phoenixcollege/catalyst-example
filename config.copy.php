<?php
return [
    'testing' => true,
    'catalyst' => [
        'username' => 'username',
        'password' => 'password',
        'authUrl' => 'https://tully.rosettastone.com/oauth/token',
        'graphqlUrl' => 'https://rsm2-api.rosettastone.com/graphql',
    ],
    'database' => [
        'dsn' => 'somedsn',
        'username' => 'username',
        'password' => 'password',
    ],
    'logging' => [
        'level' => \Catalyst\Logging\Logger::INFO
    ],
];
