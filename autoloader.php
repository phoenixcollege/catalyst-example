<?php
spl_autoload_register(function (string $class) {
    $filePath = str_replace('\\', '/', $class).'.php';
    require __DIR__.'/src/'.$filePath;
});
