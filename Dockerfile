ARG PHP_VERSION=7.4
FROM php:${PHP_VERSION}-cli-alpine

VOLUME ["/app"]

WORKDIR /app

CMD [ "php" , "/app/app.php" ]
