## /src

### `Catalyst`

Requestable: provided by `Catalyst/Graphql/Mutation and Query` to `RequestHandler` to create the api request.

RequestHandler: uses the `Catalyst/Request` implementation to send a request to the api and return the Response models.

Response: a class used by `Catalyst/Request` to help with responses and errors from the api

### `Catalyst/Auth`

: authentication classes

### `Catalyst/Db`

: tiny amount of database abstraction, mostly to make the test data easier

AbstractDb: base Db provider DbInterface: Db providers implement this interface Odbc: Odbc provider Test: Test
provider (uses data from `Catalyst/Db/TestData`)

### `Catalyst/Graphql`

: classes to build string query/mutation and variable array

Variable: creates variables (and related strings) for queries/mutations

AbstractGraphql: base class for query/mutation string/variable array builders returns an instance of Requestable which
contains the query/mutation string, array of variables, and the response model to build from the CURL response

### `Catalyst/Graphql/Mutation`

: concrete classes to create mutation strings/variables

### `Catalyst/Graphql/Query`

: concrete classes to create query string/variables

### `Catalyst/Helpers/Traits`

SemesterNameTrait: helper trait using Dave's code to build semester string name

### `Catalyst/Logging`

: responsible for logging

Logger: static class that can be called to log to the handlers that have been set

LoggerTrait: helper trait that can be added to other classes to make logging simpler

### `Catalyst/Logging/Handlers`

: do the actual logging for the `Logger`

AbstractHandler: extended by concrete implementations EchoHandler: echoes logging output to the screen (cli, not browser
friendly)
NullHandler: does nothing (if you don't want a logger)
OdbcHandler: example of odbc implementation (haven't actually tried it, probably doesn't work)

### `Catalyst/Lookups`

: provide an iterable (defaults to chunks of 50 records). These can either be raw rows or converted

directly to the `Catalyst/Model/Input` model for use in the `Catalyst/Updaters/Catalyst` classes

AbstractLookup: base class for App lookups

LookupInterface: interface for App lookups

### `Catalyst/Lookups/App`

: examples of Db (odbc/test) lookups. Supposed to yield 50 records per chunk, but who knows if I

did it correctly ;)

### `Catalyst/Lookups/Catalyst`:

MyOrg: ::getId() to get the orgId Users: simple ssoId lookup helper since I reused it several times

### `Catalyst/Model`

: contains input and response (returned from graphql request to RS) models

GenericVO: base model for Input and Response models. Simple value object model.

### `Catalyst/Model/Input`

: contains "input" models. These are used to create the input variables for the mutations.

### `Catalyst/Model/Response`

: contains models that are returned after a request to the graphql api. For responses to

mutations, they generally contain a 'clientMutationId' that matches an identifier provided for each record/row from
the `Catalyst/Lookups/App` results.

### `Catalyst/Request`

: contains the classes that do the work of requesting a response from the graphql api

AbstractRequest: base class for requests

Curl: curl implementation

Test: test implementation (creates responses from the /data directory)

TestResponse: pass an array of `TestResponse` to `Test` constructor to setup the faked responses from the /data
directory

### `Catalyst/Updaters`

: classes that update either the local database or catalyst (mutation)

AbstractUpdater: base class for App and Catalyst updaters

### `Catalyst/Updaters/App`

: receive an array of the Response models which can be iterated over to update the local

tables. During testing, pass Test Db instance to the constructor to skip the db operation and just log the queries.

### `Catalyst/Updaters/Catalyst`

: receive an array of the Input models which are used to create a mutation

(using `Catalyst/Graphql/Mutation`). Some of these classes either lookup existing records (`CreateUsers`
queries the RS Users by ssoId) or use externally supplied data to limit work (`AddUsersToGroups` and
`AssignLicenses` both lookup a chunk of users by ssoId and keep track of the ones they've updated/exist).

## General steps for a lookup -> mutation -> local update

The basic flow is as follow:

1) Create a local lookup provider (`Catalyst/Lookups/App`)
2) Get the iterator `$provider->fetchNewAsChunk()`, first argument is the parameters for the query, second is the count
   per chunk, third is whether to return the raw rows or convert to the default models
3) Create the `Catalyst/Updaters/Catalyst` and `Catalyst/Updaters/App` updaters
4) iterate over the iterator created in 2
    1) handle the catalyst updater `->update($chunkFromIterator)`
    2) handle the app updater `->update($catalystResults)`
5) handle any errors in any updater `->hasErrors(): true/false` `->getErrors(): array`

This is the most basic example of how it might work. Classes/groups is pretty much exactly the above. Other parts get
more complicated but are still roughly the same.

The people iterator creates the users (if they don't exist) and registers them as well.

The enrollment iterator looks up the users in the current chunk (they should theoretically all exist by this point)
and adds them to the first updater (`AddUsersToGroups`). `AddUsersToGroups` adds any groups that users are missing and
returns the added group array. It also updates its internal 'users' array with the updated information. The app/test
updater uses the added groups response to update the local db/log. The `AssignLicenses` updater gets an updated user
array from `AddUsersToGroups` and does the same thing with licenses.


