<?php

namespace Catalyst\Lookups\Test;

use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\CreateUser;
use Catalyst\Model\Input\RegisterUser;
use Catalyst\Model\Response\User;

class People extends TestLookup
{

    protected $modelCreation = [
        RegisterUser::class => 'toRegisterUserModel',
    ];

    protected function toRegisterUserModel(User $data): ?RegisterUser
    {
        return new RegisterUser([
            'clientMutationId' => $data->clientMutationId,
            'userId' => $data->id,
            'password' => md5(random_bytes(32)),
        ]);
    }

    protected function createRow(array $params, int $index): array
    {
        $email = 'abc'.$index.'@example.edu';
        return [
            'id_person' => $index,
            'email' => $email,
            'ssoid' => $email,
            'firstName' => 'First'.$index,
            'lastName' => 'Last'.$index,
        ];
    }

    protected function toModel(array $data): ?AbstractInputModel
    {
        return new CreateUser([
            'clientMutationId' => $data['id_person'],
            'email' => $data['email'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'orgId' => $this->orgId,
            'ssoId' => $data['ssoid'],
        ]);
    }
}
