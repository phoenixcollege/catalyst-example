<?php

namespace Catalyst\Lookups\Test;

class Vars
{

    protected $vars = [
        'current_semester' => 'x224',
        'x224-LicensePoolID' => 'license-id-1',
    ];

    public function get(string $name): ?string
    {
        return $this->vars[$name] ?? null;
    }
}
