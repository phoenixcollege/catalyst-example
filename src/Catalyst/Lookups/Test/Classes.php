<?php

namespace Catalyst\Lookups\Test;

use Catalyst\Helpers\Traits\SemesterNameTrait;
use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\CreateGroup;

class Classes extends TestLookup
{

    use SemesterNameTrait;

    protected function createRow(array $params, int $index): array
    {
        return [
            'ID_Course' => $index,
            'class_semester' => 'x224',
            'class_name' => 'ABC'.(100 + $index),
            'class_section' => 10000 + $index,
        ];
    }

    protected function toModel(array $data): ?AbstractInputModel
    {
        return new CreateGroup([
            'clientMutationId' => $data['ID_Course'],
            'groupName' => $this->getSemesterName($data['class_semester']).' '.$data['class_name'].' '.$data['class_section'].' (GCC)',
        ]);
    }
}
