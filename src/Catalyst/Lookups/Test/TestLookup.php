<?php

namespace Catalyst\Lookups\Test;

use Catalyst\Lookups\AbstractLookup;

abstract class TestLookup extends AbstractLookup
{

    abstract protected function createRow(array $params, int $index): array;

    public function fetchNewAsChunk(array $params = [], int $count = 50, bool $raw = false): iterable
    {
        $rows = $this->createRows($params, $count);
        if ($raw) {
            yield $rows;
        } else {
            yield $this->toModels($rows);
        }
    }

    protected function createRows(array $params, int $count): array
    {
        $rows = [];
        for ($i = 0; $i < $count; $i++) {
            $rows[] = $this->createRow($params, $i);
        }
        return $rows;
    }
}
