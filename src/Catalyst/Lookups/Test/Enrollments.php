<?php

namespace Catalyst\Lookups\Test;

use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\AddUserToGroup;
use Catalyst\Model\Input\AssignLicense;

class Enrollments extends TestLookup
{

    /**
     * @var string
     */
    protected $licensePoolId;

    protected $modelCreation = [
        AssignLicense::class => 'toAssignLicenseModel',
    ];

    public function __construct($orgId, $licensePoolId)
    {
        parent::__construct($orgId);
        $this->licensePoolId = $licensePoolId;
    }

    protected function createRow(array $params, int $index): array
    {
        return [
            'id_enroll' => 'e'.$index,
            'id_person' => $index,
            'rs_person_id' => 'user-id-'.$index,
            'rs_course_id' => 'group-id-'.$index,
            'rs_license_id' => 'New',
            'ssoid' => 'abc'.$index.'@example.edu',
        ];
    }

    protected function toAssignLicenseModel(array $data): ?AssignLicense
    {
        return new AssignLicense([
            'clientMutationId' => $data['id_person'],
            'userId' => $data['rs_person_id'],
            'licensePoolId' => $this->licensePoolId,
        ]);
    }

    protected function toModel(array $data): ?AbstractInputModel
    {
        return new AddUserToGroup([
            'clientMutationId' => $data['id_enroll'],
            'userId' => $data['rs_person_id'],
            'groupId' => $data['rs_course_id'],
        ]);
    }
}
