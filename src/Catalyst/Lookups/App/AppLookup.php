<?php

namespace Catalyst\Lookups\App;

use Catalyst\Db\DbInterface;

abstract class AppLookup extends \Catalyst\Lookups\AbstractLookup
{

    /**
     * @var \Catalyst\Db\DbInterface
     */
    protected $connection;

    protected $loggerKey = 'APP_PROVIDER';

    public function __construct(DbInterface $connection, string $orgId)
    {
        $this->connection = $connection;
        parent::__construct($orgId);
    }

    abstract protected function getNewSql(): string;

    public function fetchNewAsChunk(array $params = [], int $count = 50, bool $raw = false): iterable
    {
        $sql = $this->getNewSql();
        $status = $this->connection->execute($sql, $params);
        foreach ($this->getResultsToChunk($count, $raw) as $chunk) {
            yield $chunk;
        }
    }

    protected function getResultsToChunk(int $count, bool $raw): iterable
    {
        $counter = 0;
        $chunk = [];
        while ($row = $this->connection->fetchNext()) {
            $chunk[] = $row;
            $counter++;
            if ($counter >= $count) {
                yield $raw ? $chunk : $this->toModels($chunk);
                $chunk = [];
                $counter = 0;
            }
        }
        if (count($chunk)) {
            yield $raw ? $chunk : $this->toModels($chunk);
        }
    }

}
