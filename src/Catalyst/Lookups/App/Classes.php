<?php

namespace Catalyst\Lookups\App;

use Catalyst\Helpers\Traits\SemesterNameTrait;
use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\CreateGroup;

class Classes extends AppLookup
{

    use SemesterNameTrait;

    protected function getName(array $data): string
    {
        return sprintf(
            '%s %s %s (GCC)',
            $this->getSemesterName($data['class_semester']),
            $data['class_name'],
            $data['class_section']
        );
    }

    protected function getNewSql(): string
    {
        return "select * from rs_class_vw where rs_course_id = 'new' and class_semester = ?";
    }



    protected function toModel(array $data): ?AbstractInputModel
    {
        if (!isset($data['ID_Course'])) {
            return null;
        }
        return new CreateGroup([
            'clientMutationId' => $data['ID_Course'],
            'groupName' => $this->getName($data),
        ]);
    }
}
