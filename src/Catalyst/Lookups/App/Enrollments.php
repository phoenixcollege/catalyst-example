<?php

namespace Catalyst\Lookups\App;

use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\AddUserToGroup;
use Catalyst\Model\Input\AssignLicense;

class Enrollments extends AppLookup
{

    /**
     * @var string
     */
    protected $licensePoolId;

    protected $modelCreation = [
        AssignLicense::class => 'toAssignLicenseModel',
    ];

    public function __construct($connection, $orgId, $licensePoolId)
    {
        parent::__construct($connection, $orgId);
        $this->licensePoolId = $licensePoolId;
    }

    protected function getNewSql(): string
    {
        return <<<SQL
select * from rs_enroll_vw e
join person p on p.pers_emplid = e.emplid
where enrl_status_reason_cd = 'ENRL' and rs_enroll_id = 'New'
and enroll_semester = ?
and len(p.rs_person_id) > 10
SQL;
    }

    protected function toAssignLicenseModel(array $data): ?AssignLicense
    {
        if (!$this->validateAssignLicense($data)) {
            return null;
        }
        return new AssignLicense([
            'clientMutationId' => $data['id_person'],
            'userId' => $data['rs_person_id'],
            'licensePoolId' => $this->licensePoolId,
        ]);
    }

    protected function toModel(array $data): ?AbstractInputModel
    {
        if (!$this->validateAddUserToGroup($data)) {
            return null;
        }
        return new AddUserToGroup([
            'clientMutationId' => $data['id_enroll'],
            'userId' => $data['rs_person_id'],
            'groupId' => $data['rs_course_id'],
        ]);
    }

    protected function validateAddUserToGroup(array $data): bool
    {
        return isset($data['id_enroll']) && isset($data['rs_person_id']) && isset($data['rs_course_id']);
    }

    protected function validateAssignLicense(array $data): bool
    {
        return isset($data['id_person']) && isset($data['rs_person_id']) && $this->licensePoolId;
    }
}
