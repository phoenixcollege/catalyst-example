<?php

namespace Catalyst\Lookups\App;

use Catalyst\Db\DbInterface;

class Vars
{

    /**
     * @var \Catalyst\Db\DbInterface
     */
    protected $connection;

    public function __construct(DbInterface $connection)
    {
        $this->connection = $connection;
    }

    public function get(string $name): ?string
    {
        $sql = "select var_value from vars where var_name = ?";
        $status = $this->connection->execute($sql, [$name]);
        $row = $this->connection->fetchNext();
        return $row['var_value'] ?? null;
    }
}
