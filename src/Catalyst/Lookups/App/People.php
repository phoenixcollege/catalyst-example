<?php

namespace Catalyst\Lookups\App;

use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Input\CreateUser;
use Catalyst\Model\Input\RegisterUser;
use Catalyst\Model\Response\User;

class People extends AppLookup
{

    protected $modelCreation = [
        RegisterUser::class => 'toRegisterUserModel',
    ];

    protected function getNewSql(): string
    {
        return "SELECT * FROM rs_person_vw WHERE rs_person_id ='New' order by id_person";
    }

    protected function toModel(array $data): ?AbstractInputModel
    {
        if (!$this->validateData($data)) {
            return null;
        }
        return new CreateUser([
            'clientMutationId' => $data['id_person'],
            'email' => $data['email'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'orgId' => $this->orgId,
            'ssoId' => $data['ssoid'],
        ]);
    }

    protected function toRegisterUserModel(User $data): ?RegisterUser
    {
        return new RegisterUser([
            'clientMutationId' => $data->clientMutationId,
            'userId' => $data->id,
            'password' => md5(random_bytes(32)),
        ]);
    }

    protected function validateData(array $data): bool
    {
        return isset($data['id_person']) && $data['email'] && $data['ssoid'];
    }
}
