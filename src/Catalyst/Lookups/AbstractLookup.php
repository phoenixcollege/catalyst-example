<?php

namespace Catalyst\Lookups;

use Catalyst\Logging\LoggerTrait;
use Catalyst\Model\Input\AbstractInputModel;

abstract class AbstractLookup implements LookupInterface
{

    use LoggerTrait;

    protected $modelCreation = [
        //CreateGroup::class => 'toCreateGroupModel',
    ];

    /**
     * @var string
     */
    protected $orgId;

    public function __construct($orgId)
    {
        $this->orgId = $orgId;
    }

    abstract protected function toModel(array $data): ?AbstractInputModel;

    public function toModels(array $chunk, ?string $model = null): array
    {
        $models = [];
        foreach ($chunk as $row) {
            $m = $this->handleInputModelCreation($row, $model);
            if ($m) {
                $models[] = $this->handleInputModelCreation($row, $model);
            }
        }
        return $models;
    }

    protected function handleInputModelCreation($row, ?string $model): ?AbstractInputModel
    {
        if ($model === null) {
            return $this->toModel($row);
        }
        $method = $this->modelCreation[$model];
        if ($method && method_exists($this, $method)) {
            return $this->$method($row);
        }
        throw new \OutOfBoundsException('No model creation method found for '.$model);
    }
}
