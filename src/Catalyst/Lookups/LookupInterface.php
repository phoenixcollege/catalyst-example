<?php

namespace Catalyst\Lookups;

interface LookupInterface
{

    public function fetchNewAsChunk(array $params = [], int $count = 50, bool $raw = false): iterable;

    public function toModels(array $chunk, ?string $model = null): array;
}
