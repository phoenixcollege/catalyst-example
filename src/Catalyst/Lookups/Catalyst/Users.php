<?php

namespace Catalyst\Lookups\Catalyst;

use Catalyst\Graphql\Query\User;
use Catalyst\RequestHandler;

class Users
{

    /**
     * @var \Catalyst\RequestHandler
     */
    protected $requestHandler;

    public function __construct(RequestHandler $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    public function getBySsoIds(array $ssoIds): array
    {
        $ssoIds = array_filter(array_unique($ssoIds));
        $userQuery = new User();
        return $this->requestHandler->fromRequestable($userQuery->bySsoIds($ssoIds));
    }
}
