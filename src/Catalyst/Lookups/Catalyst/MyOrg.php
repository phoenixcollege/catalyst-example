<?php

namespace Catalyst\Lookups\Catalyst;

use Catalyst\RequestHandler;

class MyOrg
{

    protected $requestHandler;

    public function __construct(RequestHandler $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    public function getId(): ?string
    {
        $myOrgQuery = new \Catalyst\Graphql\Query\MyOrg();
        $myOrg = $this->requestHandler->fromRequestable($myOrgQuery->myOrg())[0] ?? null;
        if (!$myOrg || !$myOrg->id) {
            throw new \Exception('OrgId not found');
        }
        return $myOrg->id;
    }
}
