<?php

namespace Catalyst\Updaters;

abstract class AbstractUpdater
{

    protected $errors = [];

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }
}
