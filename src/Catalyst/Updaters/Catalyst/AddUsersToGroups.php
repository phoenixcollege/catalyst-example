<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;

class AddUsersToGroups extends CatalystUpdater
{

    use UsesUsersTrait;

    protected function getMutationBuilder(): AbstractMutation
    {
        return new \Catalyst\Graphql\Mutation\AddUserToGroup();
    }

    protected function hasUserAndGroupId(\Catalyst\Model\Input\AddUserToGroup $model): bool
    {
        $existing = $this->getUserById($model->userId);
        if ($existing) {
            return in_array($model->groupId, $existing->groupIds);
        }
        return false;
    }

    protected function limitToMissing(array $models): array
    {
        $missing = [];
        foreach ($models as $model) {
            if (!$this->hasUserAndGroupId($model)) {
                $missing[] = $model;
            }
        }
        return $missing;
    }

    protected function postUpdate(array $models, array $responseModels)
    {
        $this->addUsers($responseModels);
        return $this->getUsers();
    }
}
