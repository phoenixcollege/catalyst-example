<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;
use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Model\GenericVO;
use Catalyst\Requestable;
use Catalyst\RequestHandler;

abstract class CatalystUpdater extends \Catalyst\Updaters\AbstractUpdater
{

    use LoggerTrait;

    protected $existing = [];

    protected $loggerKey = 'CATALYST-UPDATER';

    protected $requestHandler;

    public function __construct(RequestHandler $requestHandler)
    {
        $this->requestHandler = $requestHandler;
    }

    abstract protected function getMutationBuilder(): AbstractMutation;

    public function update(array $models): array
    {
        $models = $this->preUpdate($models);
        $this->existing = [];
        $models = $this->limitToMissing($models);
        $this->log($this->existing, 'existingModels', Logger::DEBUG);
        $requestable = $this->getRequestable($models);
        $responseModels = $this->requestHandler->fromRequestable($requestable);
        $responseModels = array_merge($responseModels, $this->existing);
        $this->log($responseModels, 'allModels', Logger::DEBUG);
        $this->checkMissing($models, $responseModels);
        return $this->postUpdate($models, $responseModels);
    }

    protected function checkForMatchingModel(GenericVO $model, array $responseModels): bool
    {
        foreach ($responseModels as $responseModel) {
            $clientMutationId = (string) $model->clientMutationId;
            if (strlen($clientMutationId) && $clientMutationId === (string) $responseModel->clientMutationId) {
                return true;
            }
        }
        return false;
    }

    protected function checkMissing(array $models, array $responseModels, string $key = 'MISSING')
    {
        foreach ($models as $model) {
            if (!$this->checkForMatchingModel($model, $responseModels)) {
                $this->errors[$key][] = (string) $model;
            }
        }
    }

    protected function getRequestable(array $models): Requestable
    {
        return $this->getMutationBuilder()->createMany($models);
    }

    protected function limitToMissing(array $models): array
    {
        return $models;
    }

    protected function postUpdate(array $models, array $responseModels)
    {
        return $responseModels;
    }

    protected function preUpdate(array $models): array
    {
        return $models;
    }
}
