<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Model\Response\User;

trait UsesUsersTrait
{

    protected $users = [];

    public function addUsers(array $users)
    {
        foreach ($users as $user) {
            $this->users[$user->id] = $user;
        }
    }

    public function getUsers(): array
    {
        return $this->users;
    }

    protected function getUserById(string $id): ?User
    {
        return $this->users[$id] ?? null;
    }
}
