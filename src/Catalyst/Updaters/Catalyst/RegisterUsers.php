<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;
use Catalyst\Graphql\Mutation\RegisterUser;

class RegisterUsers extends CatalystUpdater
{

    protected function getMutationBuilder(): AbstractMutation
    {
        return new RegisterUser();
    }

    protected function limitToMissing(array $models): array
    {
        $notActive = [];
        foreach ($models as $user) {
            if ($user->status !== 'ACTIVE') {
                $notActive[] = $user;
            }
        }
        return $notActive;
    }
}
