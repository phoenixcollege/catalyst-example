<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;
use Catalyst\Graphql\Mutation\CreateGroup;

class CreateGroups extends CatalystUpdater
{

    protected function getMutationBuilder(): AbstractMutation
    {
        return new CreateGroup();
    }
}
