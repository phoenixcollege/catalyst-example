<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;
use Catalyst\Graphql\Mutation\AssignLicense;

class AssignLicenses extends CatalystUpdater
{

    use UsesUsersTrait;

    protected function getMutationBuilder(): AbstractMutation
    {
        return new AssignLicense();
    }

    protected function hasUserAndLicenseId(\Catalyst\Model\Input\AssignLicense $model): bool
    {
        $existing = $this->getUserById($model->userId);
        if ($existing) {
            foreach ($existing->licenses as $license) {
                $licensePool = $license->licensePool;
                if ($licensePool && $licensePool->id === $model->licensePoolId) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function limitToMissing(array $models): array
    {
        $missing = [];
        foreach ($models as $model) {
            if (!$this->hasUserAndLicenseId($model)) {
                $missing[] = $model;
            }
        }
        return $missing;
    }

    protected function postUpdate(array $models, array $responseModels)
    {
        $this->addUsers($responseModels);
        return $this->getUsers();
    }
}
