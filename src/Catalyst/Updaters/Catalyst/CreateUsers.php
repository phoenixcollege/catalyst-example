<?php

namespace Catalyst\Updaters\Catalyst;

use Catalyst\Graphql\Mutation\AbstractMutation;
use Catalyst\Graphql\Mutation\CreateUser;
use Catalyst\Graphql\Query\User;
use Catalyst\Lookups\Catalyst\Users;
use Catalyst\Model\Response\ResponseVO;

class CreateUsers extends CatalystUpdater
{

    protected function getCatalystModelsBySsoId(array $models): array
    {
        $ssoIds = $this->getSsoIdsFromModels($models);
        $usersLookup = new Users($this->requestHandler);
        return $usersLookup->getBySsoIds($ssoIds);
    }

    protected function getMatchingCatalystModel(
        \Catalyst\Model\Input\CreateUser $user,
        array $catalystModels
    ): ?ResponseVO {
        foreach ($catalystModels as $catalystModel) {
            if ($user->ssoId === $catalystModel->ssoId) {
                return $catalystModel;
            }
        }
        return null;
    }

    protected function getMutationBuilder(): AbstractMutation
    {
        return new CreateUser();
    }

    protected function getSsoIdsFromModels(array $models): array
    {
        $ssoIds = [];
        foreach ($models as $model) {
            $ssoIds[$model->ssoId] = $model->ssoId;
        }
        return array_values($ssoIds);
    }

    protected function limitToMissing(array $models): array
    {
        $missing = [];
        $catalystModels = $this->getCatalystModelsBySsoId($models);
        foreach ($models as $model) {
            $catalystModel = $this->getMatchingCatalystModel($model, $catalystModels);
            if (!$catalystModel) {
                $missing[] = $model;
            } else {
                $existingModel = $this->getMutationBuilder()->getResponseModel()->newInstance($model->toArray());
                $existingModel->setAttributes($catalystModel->toArray());
                $this->existing[] = $existingModel;
            }
        }
        return $missing;
    }
}
