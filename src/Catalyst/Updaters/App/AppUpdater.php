<?php

namespace Catalyst\Updaters\App;

use Catalyst\Db\DbInterface;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Model\Response\ResponseVO;

abstract class AppUpdater extends \Catalyst\Updaters\AbstractUpdater
{

    use LoggerTrait;

    /**
     * @var \Catalyst\Db\DbInterface
     */
    protected $connection;

    protected $loggerKey = 'APP_UPDATER';

    public function __construct(DbInterface $connection)
    {
        $this->connection = $connection;
    }

    abstract protected function getParamsFromModel(ResponseVO $model): array;

    abstract protected function getUpdateSql(): string;

    public function update(array $models)
    {
        foreach ($models as $model) {
            $this->updateModel($model);
        }
    }

    protected function executeQuery(string $sql, array $params): bool
    {
        $status = $this->connection->execute($sql, $params, false);
        if (!$status) {
            $this->errors['ODBC'][] = $this->connection->lastError();
        }
        return $status;
    }

    protected function updateModel(ResponseVO $model)
    {
        $sql = $this->getUpdateSql();
        $params = $this->getParamsFromModel($model);
        return $this->executeQuery($sql, $params);
    }
}
