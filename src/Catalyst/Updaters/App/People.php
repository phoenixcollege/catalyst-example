<?php

namespace Catalyst\Updaters\App;

use Catalyst\Model\Response\ResponseVO;
use Catalyst\Model\Response\User;

class People extends AppUpdater
{

    public function updateLicenses(array $models, string $licenseId)
    {
        foreach ($models as $model) {
            $this->updateLicenseModel($model, $licenseId);
        }
    }

    protected function getLicenseParamsFromModel(User $model, string $licenseId): array
    {
        return [
            $licenseId,
            $model->id,
        ];
    }

    protected function getLicenseUpdateSql(): string
    {
        return "update person set rs_license_id = ?, rs_person_update = getdate() where rs_person_id = ?";
    }

    protected function getParamsFromModel(ResponseVO $model): array
    {
        return [
            $model->id,
            $model->clientMutationId,
        ];
    }

    protected function getUpdateSql(): string
    {
        return "update person set rs_person_id = ?, rs_person_update = getdate() where id_person = ?";
    }

    protected function updateLicenseModel(User $model, string $licenseId)
    {
        $sql = $this->getLicenseUpdateSql();
        $params = $this->getLicenseParamsFromModel($model, $licenseId);
        return $this->executeQuery($sql, $params);
    }
}
