<?php

namespace Catalyst\Updaters\App;

use Catalyst\Model\Response\ResponseVO;

class Enrollments extends AppUpdater
{

    protected function getParamsFromModel(ResponseVO $model): array
    {
        return [
            'Assigned',
            $model->clientMutationId,
        ];
    }

    protected function getUpdateSql(): string
    {
        return <<<SQL
update enroll 
set rs_enroll_id = ?,
rs_enroll_update = getdate()
where id_enroll = ?
SQL;
    }
}
