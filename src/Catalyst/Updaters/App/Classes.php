<?php

namespace Catalyst\Updaters\App;

use Catalyst\Model\Response\ResponseVO;

class Classes extends AppUpdater
{

    protected function getParamsFromModel(ResponseVO $model): array
    {
        return [
            $model->id,
            $model->clientMutationId,
        ];
    }

    protected function getUpdateSql(): string
    {
        return "update class set rs_course_id = ?, rs_class_update = getdate() where id_course = ?";
    }
}
