<?php

namespace Catalyst;

use Catalyst\Model\Response\ResponseVO;

class Requestable
{

    /**
     * @var string
     */
    public $query;

    /**
     * @var ResponseVO
     */
    public $responseModel;

    /**
     * @var \Catalyst\Graphql\Variable[]
     */
    public $variables = [];

    public function __construct(string $query, array $variables, ResponseVO $responseModel)
    {
        $this->query = $query;
        $this->variables = $variables;
        $this->responseModel = $responseModel;
    }

    public function toVariableArray(): array
    {
        $vars = [];
        /** @var \Catalyst\Graphql\Variable $variable */
        foreach ($this->variables as $variable) {
            $vars[$variable->getVariableKey(false)] = $variable->value;
        }
        return $vars;
    }
}
