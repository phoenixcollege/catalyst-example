<?php

namespace Catalyst\Logging\Handlers;

class EchoHandler extends AbstractHandler
{

    protected function handle(int $step, string $key, string $data, int $level)
    {
        echo sprintf('[%s][%d] %d: (%s) %s', microtime(true), $level, $step, $key, $data).PHP_EOL;
    }
}
