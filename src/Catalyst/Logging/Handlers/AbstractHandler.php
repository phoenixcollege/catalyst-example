<?php

namespace Catalyst\Logging\Handlers;

abstract class AbstractHandler
{

    abstract protected function handle(int $step, string $key, string $data, int $level);

    public function write(int $step, string $key, $data, int $level)
    {
        if (is_array($data)) {
            $data = json_encode($data);
        }
        if (is_object($data)) {
            $data = $this->handleObject($data);
        }
        $this->handle($step, $key, (string) $data, $level);
    }

    protected function handleArray(array $data): string
    {
        return json_encode($data);
    }

    protected function handleObject($data): string
    {
        if (method_exists($data, 'toJson')) {
            return $data->toJson();
        }
        if (method_exists($data, 'toArray')) {
            return $this->handleArray($data->toArray());
        }
        return (string) $data;
    }
}
