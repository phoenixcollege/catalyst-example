<?php

namespace Catalyst\Logging\Handlers;

class NullHandler extends AbstractHandler
{

    public function write(int $step, string $key, $data, int $level)
    {
        // do nothing, save cpu :)
    }

    protected function handle(int $step, string $key, string $data, int $level)
    {
        // do nothing
    }
}
