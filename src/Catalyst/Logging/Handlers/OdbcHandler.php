<?php

namespace Catalyst\Logging\Handlers;

class OdbcHandler extends AbstractHandler
{

    protected $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function __destruct()
    {
        if ($this->connection) {
            odbc_close($this->connection);
        }
        $this->connection = null;
    }

    protected function handle(int $step, string $key, string $data, int $level)
    {
        $sql = "insert into task_log values(?, ?, ?, ?, ?)";
        $handler = odbc_prepare($this->connection, $sql);
        return odbc_execute($handler, [
            $step,
            $data,
            "??",
            getdate(),
            $key,
        ]);
    }
}
