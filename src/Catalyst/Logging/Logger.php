<?php

namespace Catalyst\Logging;

use Catalyst\Logging\Handlers\NullHandler;

class Logger
{

    const DEBUG = 1;
    const INFO = 2;

    protected static $count = 0;

    protected static $handlers = [];

    protected static $initialized = false;

    protected static $level = Logger::INFO;

    protected static $skipKeys = [];

    public static function setLevel(int $level)
    {
        self::$level = $level;
    }

    public static function setSkipKeys(array $keys)
    {
        self::$skipKeys = $keys;
    }

    public static function write($data, string $key = 'ALL', int $level = Logger::INFO)
    {
        if (self::shouldLog($data, $key, $level)) {
            self::initialize();
            foreach (self::getHandlers() as $index => $handler) {
                try {
                    $handler->write(self::$count, $key, $data, $level);
                } catch (\Throwable $e) {
                    //unset(self::$handlers[$index]);
                    var_dump($e);
                }
            }
            self::$count++;
        }
    }

    protected static function getHandlers(): array
    {
        if (!self::$handlers) {
            self::setHandlers([new NullHandler()]);
        }
        return self::$handlers;
    }

    public static function setHandlers(array $handlers)
    {
        self::$handlers = $handlers;
    }

    protected static function initialize()
    {
        if (!self::$initialized) {
            self::$initialized = true;
            self::write('Logger initialized', 'LOGGER');
        }
    }

    protected static function shouldLog($data, string $key, int $level): bool
    {
        if ($level < self::$level) {
            return false;
        }
        $check = explode(':', $key)[0];
        return !in_array($check, self::$skipKeys);
    }
}
