<?php

namespace Catalyst\Logging;

trait LoggerTrait
{

    protected function getLoggerKey($key = null): string
    {
        $fullKey = [null, get_class($this), $key];
        if (property_exists($this, 'loggerKey')) {
            $fullKey[0] = $this->loggerKey;
        }
        return implode(':', array_filter($fullKey));
    }

    protected function log($data, $key = null, int $level = Logger::INFO)
    {
        Logger::write($data, $this->getLoggerKey($key), $level);
    }
}
