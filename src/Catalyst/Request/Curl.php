<?php

namespace Catalyst\Request;

use Catalyst\Response;

class Curl extends AbstractRequest
{

    protected function doRequest($body, array $headers): Response
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        if ($curlErrNo = curl_errno($ch)) {
            throw new \Exception(curl_error($ch), $curlErrNo);
        }
        curl_close($ch);
        return new Response($statusCode, $result);
    }
}
