<?php

namespace Catalyst\Request;

use Catalyst\Logging\Logger;
use Catalyst\Response;

class Test extends AbstractRequest
{

    protected $responses = [];

    public function __construct($url, array $responses)
    {
        $url = $this->ensureUrlEndsWithSlash($url);
        parent::__construct($url);
        $this->responses = $responses;
    }

    public function addResponse(TestResponse $response)
    {
        $this->responses[] = $response;
    }

    protected function doRequest($body, array $headers): Response
    {
        $response = $this->getCurrentPathAndStatus();
        $this->log($response, 'testResponse', Logger::DEBUG);
        $content = file_get_contents($this->getPath($response));
        return new Response($response->statusCode, $content);
    }

    protected function ensureUrlEndsWithSlash(string $url): string
    {
        if (substr($url, -1) !== DIRECTORY_SEPARATOR) {
            $url = $url.DIRECTORY_SEPARATOR;
        }
        return $url;
    }

    protected function getCurrentPathAndStatus(): TestResponse
    {
        $response = array_shift($this->responses);
        if (!$response) {
            throw new \OutOfBoundsException("No more responses remaining.");
        }
        return $response;
    }

    protected function getPath(TestResponse $response): string
    {
        return $this->url.$response->path;
    }
}
