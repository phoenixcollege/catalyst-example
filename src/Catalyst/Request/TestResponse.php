<?php

namespace Catalyst\Request;

class TestResponse
{

    /**
     * @var string
     */
    public $path;

    /**
     * @var int
     */
    public $statusCode;

    public function __construct(string $path, int $statusCode = 200)
    {
        $this->path = $this->ensurePathDoesNotStartWithSlash($path);
        $this->statusCode = $statusCode;
    }

    public function __toString()
    {
        return sprintf('%d: %s', $this->statusCode, $this->path);
    }

    protected function ensurePathDoesNotStartWithSlash(string $path): string
    {
        if (substr($path, 0, 1) === DIRECTORY_SEPARATOR) {
            $path = substr($path, 1);
        }
        return $path;
    }
}
