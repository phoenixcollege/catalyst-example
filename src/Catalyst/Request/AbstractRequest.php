<?php

namespace Catalyst\Request;

use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Response;

abstract class AbstractRequest
{

    use LoggerTrait;

    /**
     * @var string|array
     */
    public $url;

    protected $loggerKey = 'REQUEST';

    public function __construct($url)
    {
        $this->url = $url;
    }

    abstract protected function doRequest($body, array $headers): Response;

    public function request($body, array $headers = []): Response
    {
        $headers = $this->ensureContentTypeHeader($headers);
        $this->log($headers, 'headers', Logger::DEBUG);
        $this->log($body, 'body', Logger::DEBUG);
        $response = $this->doRequest($body, $headers);
        $this->log($response, 'response', Logger::DEBUG);
        return $response;
    }

    protected function ensureContentTypeHeader(array $headers): array
    {
        $found = false;
        foreach ($headers as $header) {
            if (stripos($header, 'content-type') !== false) {
                $found = true;
            }
        }
        if (!$found) {
            $headers[] = 'Content-Type: application/json';
        }
        return $headers;
    }
}
