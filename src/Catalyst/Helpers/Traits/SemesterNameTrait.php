<?php

namespace Catalyst\Helpers\Traits;

trait SemesterNameTrait
{

    protected function getSemesterName(string $semester): string
    {
        $yr = substr($semester, 1, 2);
        $sem_code = substr($semester, 3);
        if ($sem_code == "2") {
            $sem_name = "20".$yr." Spring";
        }
        if ($sem_code == "4") {
            $sem_name = "20".$yr." Summer";
        }
        if ($sem_code == "6") {
            $sem_name = "20".$yr." Fall";
        }
        return $sem_name;
    }
}
