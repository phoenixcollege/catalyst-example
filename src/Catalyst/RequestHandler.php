<?php

namespace Catalyst;

use Catalyst\Request\AbstractRequest;

class RequestHandler
{

    /**
     * @var \Catalyst\Request\AbstractRequest
     */
    public $request;

    /**
     * @var \Catalyst\Auth\Token
     */
    public $token;

    public function __construct(AbstractRequest $request, $token)
    {
        $this->request = $request;
        $this->token = $token;
    }

    public function bodyFromRequestable(Requestable $requestable): string
    {
        $data = ['query' => $requestable->query];
        $variables = $requestable->toVariableArray();
        if ($variables) {
            $data['variables'] = $variables;
        }
        return json_encode($data);
    }

    /**
     * @param  \Catalyst\Requestable  $requestable
     * @param  array  $headers
     * @return \Catalyst\Model\Response\ResponseVO[]|null
     * @throws \Exception
     */
    public function fromRequestable(Requestable $requestable, array $headers = [])
    {
        $headers[] = $this->token->toHeader();
        $response = $this->request->request($this->bodyFromRequestable($requestable), $headers);
        return $this->convertToModelOrModels($requestable, $response);
    }

    protected function convertToModelOrModels(Requestable $requestable, Response $response)
    {
        $data = $response->data();
        return $requestable->responseModel->createModelsFromData($data);
    }
}
