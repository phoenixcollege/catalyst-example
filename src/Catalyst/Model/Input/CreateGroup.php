<?php

namespace Catalyst\Model\Input;

class CreateGroup extends AbstractInputModel
{

    public $type = 'CreateGroupInput!';

    protected $attributes = [
        'groupDescription' => 'Created by GCC Interface',
    ];

    protected $required = [
        'clientMutationId',
        'groupName',
        'groupDescription',
    ];
}
