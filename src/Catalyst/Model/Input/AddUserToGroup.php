<?php

namespace Catalyst\Model\Input;

class AddUserToGroup extends AbstractInputModel
{

    public $type = 'AddUserToGroupInput!';

    protected $attributes = [];

    protected $required = [
        'clientMutationId',
        'groupId',
        'userId',
    ];
}
