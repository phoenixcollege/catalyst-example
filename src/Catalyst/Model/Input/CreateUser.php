<?php

namespace Catalyst\Model\Input;

class CreateUser extends AbstractInputModel
{

    protected $attributes = [
        'interfaceLanguage' => 'en_US',
        'languageOfStudy' => 'LET_LEARNER_SELECT',
        'notes' => 'Created by Interface',
        'userField1' => 'GCC',
    ];

    protected $required = [
        'clientMutationId',
        'email',
        'firstName',
        'interfaceLanguage',
        'languageOfStudy',
        'lastName',
        'orgId',
        'ssoId',
    ];

    public $type = 'CreateUserInput!';
}
