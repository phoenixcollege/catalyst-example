<?php

namespace Catalyst\Model\Input;

class RegisterUser extends AbstractInputModel
{

    public $type = 'RegisterUserInput!';

    protected $attributes = [
        'voiceType' => 'FEMALE',
        'originLanguage' => 'en-US',
        'timezone' => 'America/Phoenix',
    ];

    protected $required = [
        'clientMutationId',
        'voiceType',
        'originLanguage',
        'timezone',
        'userId',
    ];
}
