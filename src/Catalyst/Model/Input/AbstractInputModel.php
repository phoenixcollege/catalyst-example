<?php

namespace Catalyst\Model\Input;

use Catalyst\Graphql\Variable;
use Catalyst\Model\GenericVO;

abstract class AbstractInputModel extends GenericVO
{

    public $key = 'input';

    public $type = 'String!';

    protected $required = [];

    public function toArray(): array
    {
        $this->ensureComplete();
        return $this->attributes;
    }

    public function toVariable($count = null): Variable
    {
        return new Variable($this->key, $this->toArray(), $count, $this->type);
    }

    protected function ensureComplete()
    {
        foreach ($this->required as $key) {
            if (!isset($this->attributes[$key])) {
                throw new \Exception("$key is a required attribute.");
            }
        }
    }
}
