<?php

namespace Catalyst\Model\Input;

class AssignLicense extends AbstractInputModel
{

    public $type = 'AssignLicenseInput!';

    protected $attributes = [];

    protected $required = [
        'clientMutationId',
        'licensePoolId',
        'userId',
    ];
}
