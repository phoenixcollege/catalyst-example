<?php

namespace Catalyst\Model;

class GenericVO implements \JsonSerializable
{

    protected $attributes = [];

    public function __construct(array $attributes = [])
    {
        $this->setAttributes($attributes);
    }

    public function __get(string $key)
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, $value)
    {
        $this->setAttribute($key, $value);
    }

    public function __toString()
    {
        return json_encode($this->attributes);
    }

    public function getAttribute(string $key)
    {
        if ($this->hasGetAttributeMutator($key)) {
            $method = $this->getAttributeMutatorMethod($key);
            return $this->$method();
        }
        return $this->attributes[$key] ?? null;
    }

    public function getAttributes(): array
    {
        $attributes = [];
        foreach ($this->attributes as $key => $value) {
            $attributes[$key] = $this->getAttribute($key);
        }
        return $attributes;
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    public function jsonSerialize()
    {
        return $this->getAttributes();
    }

    public function newInstance(array $attributes = [])
    {
        return new static($attributes);
    }

    public function setAttribute(string $key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function toArray(): array
    {
        return $this->getAttributes();
    }

    protected function getAttributeMutatorMethod(string $key): string
    {
        return 'get'.ucfirst($key).'Attribute';
    }

    protected function hasGetAttributeMutator(string $key): bool
    {
        return method_exists($this, $this->getAttributeMutatorMethod($key));
    }
}
