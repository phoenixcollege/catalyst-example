<?php

namespace Catalyst\Model\Response;

use Catalyst\Model\GenericVO;

class User extends ResponseVO
{

    protected $attributeKey = 'user';

    public function getGroupIdsAttribute(): array
    {
        return $this->attributes['groupIds'] ?? [];
    }

    public function getLicensesAttribute(): array
    {
        $licenses = [];
        foreach ($this->attributes['licenses'] ?? [] as $license) {
            if ($license instanceof GenericVO) {
                $license = $license->getAttributes();
            }
            $licenses[] = new License($license);
        }
        return $licenses;
    }
}
