<?php

namespace Catalyst\Model\Response;

class License extends ResponseVO
{

    public function getLicensePoolAttribute(): LicensePool
    {
        return new LicensePool($this->attributes['licensePool'] ?? []);
    }
}
