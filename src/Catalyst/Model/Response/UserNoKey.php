<?php

namespace Catalyst\Model\Response;

class UserNoKey extends User
{

    protected $attributeKey = null;
}
