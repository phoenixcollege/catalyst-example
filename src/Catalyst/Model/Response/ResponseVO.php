<?php

namespace Catalyst\Model\Response;

use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Model\GenericVO;

class ResponseVO extends GenericVO
{

    use LoggerTrait;

    protected $attributeKey = null;

    protected $loggerKey = 'RESPONSEVO';

    public function createModelsFromData(array $data): array
    {
        $models = [];
        foreach ($this->getArrayOfAttributesFromData($data) as $index => $attributes) {
            $this->log($attributes, 'attributes', Logger::DEBUG);
            if ($this->hasDataModelAttributes($attributes)) {
                $model = $this->newInstance($attributes);
                $this->log($model, 'model', Logger::DEBUG);
                $models[] = $model;
            }
        }
        return $models;
    }

    /**
     * Override to create an array of index => attributes for each model
     *
     * @param  array  $data
     * @return array
     */
    protected function getArrayOfAttributesFromData(array $data): array
    {
        if ($this->attributeKey === null) {
            return $data;
        }
        $attributes = [];
        foreach ($data as $modelData) {
            $extra = [];
            if (isset($modelData['clientMutationId'])) {
                $extra = ['clientMutationId' => $modelData['clientMutationId']];
            }
            if ($this->attributeKey) {
                $modelData = $modelData[$this->attributeKey] ?? [];
            }
            $attributes[] = array_merge($extra, $modelData);
        }
        return $attributes;
    }

    protected function hasDataModelAttributes(array $attributes): bool
    {
        foreach ($attributes as $key => $value) {
            if ($value) {
                return true;
            }
        }
        return false;
    }
}
