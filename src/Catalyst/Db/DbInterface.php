<?php

namespace Catalyst\Db;

interface DbInterface
{

    public static function getInstance(bool $refresh = false): DbInterface;

    public static function setConnectCredentials(string $dsn, ?string $username, ?string $password);

    public function execute(string $sql, array $params = [], bool $hasResults = true): bool;

    public function fetchNext();

    public function lastError(): ?string;
}
