<?php

namespace Catalyst\Db;

use Catalyst\Logging\Logger;

class Test extends AbstractDb
{

    /**
     * @var \Catalyst\Db\TestData\TestDataInterface|null
     */
    protected $current;

    protected $testData = [];

    public function addTestData(array $testData)
    {
        foreach ($testData as $test) {
            $this->testData[] = $test;
        }
    }

    public function execute(string $sql, array $params = [], bool $hasResults = true): bool
    {
        $this->log($sql, 'sql', Logger::DEBUG);
        $this->log($params, 'params', Logger::DEBUG);
        if ($hasResults) {
            $this->setCurrentData();
            $this->log($this->current, 'currentData', Logger::DEBUG);
        }
        return true;
    }

    public function fetchNext()
    {
        if (!$this->current) {
            throw new \OutOfBoundsException('No more test data found');
        }
        return $this->current->next();
    }

    protected function cleanup()
    {
        // do nothing
    }

    protected function setCurrentData()
    {
        $this->current = array_shift($this->testData);
    }
}
