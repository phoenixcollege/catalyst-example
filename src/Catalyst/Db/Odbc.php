<?php

namespace Catalyst\Db;

use Catalyst\Logging\Logger;

class Odbc extends AbstractDb
{

    protected $connection;

    protected $resultHandler;

    public function __construct()
    {
        $this->log('Creating new instance', 'construct', Logger::DEBUG);
        $this->connection = odbc_connect(self::$credentials['dsn'], self::$credentials['username'],
            self::$credentials['password']);
    }

    public function execute(string $sql, array $params = [], bool $hasResults = true): bool
    {
        $this->log($sql, 'sql', Logger::DEBUG);
        $this->log($params, 'params', Logger::DEBUG);
        $this->cleanupResultHandler();
        $this->resultHandler = odbc_prepare($this->connection, $sql);
        $status = odbc_execute($this->resultHandler, $params);
        $this->log($status, 'status', Logger::DEBUG);
        if (!$hasResults) {
            $this->cleanupResultHandler();
        }
        return $status;
    }

    public function fetchNext()
    {
        if ($this->resultHandler) {
            return odbc_fetch_array($this->resultHandler);
        }
        return null;
    }

    public function lastError(): ?string
    {
        return odbc_error($this->connection);
    }

    protected function cleanup()
    {
        $this->cleanupResultHandler();
        if ($this->connection) {
            odbc_close($this->connection);
            $this->connection = null;
        }
    }

    protected function cleanupResultHandler()
    {
        if ($this->resultHandler) {
            odbc_free_result($this->resultHandler);
            $this->resultHandler = null;
        }
    }
}
