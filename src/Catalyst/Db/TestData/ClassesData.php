<?php

namespace Catalyst\Db\TestData;

class ClassesData extends TestDataAbstract
{

    protected function createData(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->data[] = $this->createRow($i);
        }
    }

    protected function createRow(int $index): array
    {
        return [
            'ID_Course' => $index,
            'class_semester' => 'x224',
            'class_name' => 'ABC'.(100 + $index),
            'class_section' => 10000 + $index,
        ];
    }
}
