<?php

namespace Catalyst\Db\TestData;

class EmptyData extends TestDataAbstract
{

    protected function createData(int $count)
    {
        $this->data = [];
    }
}
