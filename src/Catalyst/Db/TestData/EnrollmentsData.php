<?php

namespace Catalyst\Db\TestData;

class EnrollmentsData extends TestDataAbstract
{

    protected function createData(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->data[] = $this->createRow($i);
        }
    }

    protected function createRow(int $index): array
    {
        return [
            'id_enroll' => 'e'.$index,
            'id_person' => $index,
            'rs_person_id' => 'user-id-'.$index,
            'rs_course_id' => 'group-id-'.$index,
            'rs_license_id' => 'New',
            'ssoid' => 'abc'.$index.'@example.edu',
        ];
    }
}
