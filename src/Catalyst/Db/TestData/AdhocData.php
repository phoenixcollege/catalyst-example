<?php

namespace Catalyst\Db\TestData;

class AdhocData extends TestDataAbstract
{

    public function __construct(array $data, int $count = 10)
    {
        $this->data = $data;
        parent::__construct($count);
    }

    protected function createData(int $count)
    {
        // do nothing
    }
}
