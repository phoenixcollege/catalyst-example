<?php

namespace Catalyst\Db\TestData;

class PeopleData extends TestDataAbstract
{

    protected function createData(int $count)
    {
        for ($i = 0; $i < $count; $i++) {
            $this->data[] = $this->createRow($i);
        }
    }

    protected function createRow(int $index): array
    {
        $email = 'abc'.$index.'@example.edu';
        return [
            'id_person' => $index,
            'email' => $email,
            'ssoid' => $email,
            'firstName' => 'First'.$index,
            'lastName' => 'Last'.$index,
        ];
    }
}
