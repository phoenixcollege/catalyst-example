<?php

namespace Catalyst\Db\TestData;

abstract class TestDataAbstract implements TestDataInterface
{

    protected $data = [];

    public function __construct(int $count = 20)
    {
        $this->createData($count);
    }

    abstract protected function createData(int $count);

    public function __toString()
    {
        return json_encode($this->data);
    }

    public function next()
    {
        return array_shift($this->data);
    }
}
