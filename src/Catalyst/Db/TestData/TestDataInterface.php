<?php

namespace Catalyst\Db\TestData;

interface TestDataInterface
{
    public function next();
}
