<?php

namespace Catalyst\Db;

use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;

abstract class AbstractDb implements DbInterface
{

    use LoggerTrait;

    protected static $credentials = [
        'dsn' => null,
        'username' => null,
        'password' => null,
    ];

    protected static $instance;

    protected $loggerKey = 'DB';

    abstract protected function cleanup();

    public static function getInstance(bool $refresh = false): DbInterface
    {
        if (!self::$credentials['dsn']) {
            throw new \Exception('Connection credentials have not been set.');
        }
        if (!self::$instance || $refresh) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public static function setConnectCredentials(string $dsn, ?string $username, ?string $password)
    {
        self::$credentials = [
            'dsn' => $dsn,
            'username' => $username,
            'password' => $password,
        ];
    }

    public function __destruct()
    {
        $this->log('Preparing to cleanup', 'destroy', Logger::DEBUG);
        $this->cleanup();
    }

    public function lastError(): ?string
    {
        return null;
    }
}
