<?php

namespace Catalyst;

class Response
{

    public $rawResponse;

    public $statusCode;

    protected $errors = [];

    public function __construct($statusCode, $rawResponse)
    {
        $this->statusCode = $statusCode;
        $this->rawResponse = $rawResponse;
        $this->validate();
    }

    public function __toString()
    {
        return sprintf('%d: %s', $this->statusCode, $this->rawResponse);
    }

    /**
     * only interested in the data key for models/attributes
     *
     * @return array
     */
    public function data(): array
    {
        return $this->toArray()['data'] ?? [];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return $this->statusCode !== 200 || count($this->errors) > 0;
    }

    public function toArray(): array
    {
        return json_decode($this->rawResponse, true) ?? [];
    }

    protected function getErrorFromErrorArray(array $error): string
    {
        $parts = [
            $error['message'] ?? null,
            $error['path'] ?? null,
            $error['extensions']['response']['body']['error_description'] ?? 'Unknown Error',
        ];
        return implode(' ', array_filter($parts));
    }

    protected function handleErrorsForResponse(array $response)
    {
        if ($this->responseIsProbablyErrorString($response)) {
            $this->errors[] = $this->rawResponse;
        }
        if ($this->responseHasErrorDescription($response)) {
            $this->errors[] = $response['error_description'];
        }
        $this->handleErrorsWhenErrorArrayExists($response);
    }

    protected function handleErrorsWhenErrorArrayExists(array $response)
    {
        if ($this->responseHasErrorsKey($response)) {
            foreach ($response['errors'] ?? [] as $error) {
                $this->errors[] = $this->getErrorFromErrorArray($error);
            }
        }
    }

    protected function responseHasErrorDescription(array $response): bool
    {
        return isset($response['error_description']);
    }

    protected function responseHasErrorsKey(array $response): bool
    {
        return isset($response['errors']);
    }

    protected function responseIsProbablyErrorString(array $response): bool
    {
        return empty($response) && strlen($this->rawResponse);
    }

    protected function validate()
    {
        $this->handleErrorsForResponse($this->toArray());
    }
}
