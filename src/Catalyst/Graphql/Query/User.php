<?php

namespace Catalyst\Graphql\Query;

use Catalyst\Graphql\AbstractGraphql;
use Catalyst\Graphql\Variable;
use Catalyst\Model\Response\UserNoKey;
use Catalyst\Requestable;

class User extends AbstractGraphql
{

    public $ssoIdContentQuery = <<<QUERY
    userBySsoId({{contentVariables}}) {
        id
        email
        createdAt
        updatedAt
        firstName
        lastName
        ssoId
        status
        userField1
        groupIds
        licenses {
            licensePool {
                id
            }
        }
    }
QUERY;

    public $ssoIdWrapperQuery = <<<QUERY
query({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;

    protected $responseModel = UserNoKey::class;

    public function bySsoId(string $ssoId): Requestable
    {
        $variables = [
            new Variable('ssoId', $ssoId),
        ];
        return $this->buildRequestable($this->ssoIdWrapperQuery, $this->ssoIdContentQuery, $variables);
    }

    public function bySsoIds(array $ssoIds): Requestable
    {
        $variables = $this->makeVariablesForSsoIdQuery($ssoIds);
        return $this->buildRequestable($this->ssoIdWrapperQuery, $this->ssoIdContentQuery, $variables);
    }

    protected function makeVariablesForSsoIdQuery(array $ssoIds): array
    {
        $variables = [];
        $count = 0;
        foreach ($ssoIds as $ssoId) {
            $variables[] = new Variable('ssoId', $ssoId, $count);
            $count++;
        }
        return $variables;
    }
}
