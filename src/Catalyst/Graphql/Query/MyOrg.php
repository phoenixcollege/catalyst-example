<?php

namespace Catalyst\Graphql\Query;

use Catalyst\Model\Response\ResponseVO;
use Catalyst\Requestable;

class MyOrg
{

    public $query = <<<GRAPHQL
query {
    myOrg {
        id
        namespace
        name
    }
}
GRAPHQL;

    public function myOrg(): Requestable
    {
        return new Requestable($this->query, [], new ResponseVO());
    }
}
