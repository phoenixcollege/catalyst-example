<?php

namespace Catalyst\Graphql;

use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Model\Response\ResponseVO;
use Catalyst\Requestable;

abstract class AbstractGraphql
{

    use LoggerTrait;

    protected $loggerKey = 'GRAPHQL';

    protected $responseModel = ResponseVO::class;

    public function getResponseModel(): ResponseVO
    {
        return new $this->responseModel();
    }

    protected function buildRequestable(string $wrapperQuery, string $contentQuery, array $variables): Requestable
    {
        $wrapper = $this->makeWrapperQuery($wrapperQuery, $variables);
        $content = $this->makeContentQuery($contentQuery, $variables);
        $query = $this->makeQuery($wrapper, $content);
        $this->log($query, 'query', Logger::DEBUG);
        $this->log($variables, 'variables', Logger::DEBUG);
        return new Requestable($query, $variables, $this->getResponseModel());
    }

    /**
     * @param  \Catalyst\Model\Input\AbstractInputModel  $model
     * @return \Catalyst\Requestable
     */
    protected function createFromModel(AbstractInputModel $model): Requestable
    {
        $variables = [
            $model->toVariable(),
        ];
        return $this->buildRequestable($this->getWrapperQuery(), $this->getContentQuery(), $variables);
    }

    /**
     * @param  \Catalyst\Model\Input\AbstractInputModel[]  $models
     * @return \Catalyst\Requestable
     */
    protected function createFromModels(array $models): Requestable
    {
        $variables = $this->makeVariablesFromModels($models);
        return $this->buildRequestable($this->getWrapperQuery(), $this->getContentQuery(), $variables);
    }

    protected function getContentQuery(): string
    {
        if (property_exists($this, 'contentQuery')) {
            return $this->contentQuery;
        }
        return '';
    }

    protected function getWrapperQuery(): string
    {
        if (property_exists($this, 'wrapperQuery')) {
            return $this->wrapperQuery;
        }
        return '';
    }

    protected function makeContentQuery(string $query, array $variables): string
    {
        $content = [];
        $hasMulti = count($variables) > 0;
        /** @var Variable $variable */
        foreach ($variables as $index => $variable) {
            $content[] = $this->makeContentQueryFromVariable($query, $variable, $hasMulti ? $index : null);
        }
        return implode(PHP_EOL, $content);
    }

    protected function makeContentQueryFromVariable(string $query, Variable $variable, $counter): string
    {
        $index = null;
        if ($counter !== null) {
            $index = sprintf('index%d: ', $counter);
        }
        return $index.trim(str_replace('{{contentVariables}}', $this->makeContentVariableString($variable), $query));
    }

    protected function makeContentVariableString(Variable $variable): string
    {
        return sprintf('%s: %s', $variable->key, $variable->getVariableKey());
    }

    protected function makeQuery(string $wrapperQuery, string $contentQuery): string
    {
        return str_replace('{{contentQuery}}', $contentQuery, $wrapperQuery);
    }

    protected function makeVariablesFromModels(array $models): array
    {
        $variables = [];
        $count = 0;
        /** @var \Catalyst\Model\Input\AbstractInputModel $model */
        foreach ($models as $model) {
            $variables[] = $model->toVariable($count);
            $count++;
        }
        return $variables;
    }

    protected function makeWrapperQuery(string $query, array $variables): string
    {
        return str_replace('{{wrapperVariables}}', $this->makeWrapperVariableString($variables), $query);
    }

    protected function makeWrapperVariableString(array $variables): string
    {
        $parts = [];
        /** @var Variable $variable */
        foreach ($variables as $variable) {
            $parts[] = sprintf('%s: %s', $variable->getVariableKey(), $variable->type);
        }
        return implode(' ', $parts);
    }
}
