<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Model\Response\Group;

class CreateGroup extends AbstractMutation
{

    protected $responseModel = Group::class;

    public $contentQuery = <<<QUERY
    createGroup({{contentVariables}}) {
    clientMutationId
        group {
            id
            name
            description
        }
    }
QUERY;

    public $wrapperQuery = <<<QUERY
mutation CreateGroupMutation({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;
}
