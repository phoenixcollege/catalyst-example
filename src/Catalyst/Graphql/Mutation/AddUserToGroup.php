<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Model\Response\User;

class AddUserToGroup extends AbstractMutation
{
    public $contentQuery = <<<QUERY
    addUserToGroup({{contentVariables}}) {
        clientMutationId
        user {
            id
            email
            ssoId
            status
            groupIds
            licenses {
                licensePool {
                    id
                }
            }
        }
    }
QUERY;

    public $wrapperQuery = <<<QUERY
mutation AddUserToGroupMutation({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;

    protected $responseModel = User::class;
}
