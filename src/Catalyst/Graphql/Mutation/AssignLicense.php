<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Model\Response\User;

class AssignLicense extends AbstractMutation
{

    public $contentQuery = <<<QUERY
    assignLicense({{contentVariables}}) {
        clientMutationId
        user {
            id
            email
            ssoId
            status
            groupIds
            licenses {
                assignedAt
                licensePool {
                    id
                }
            }
        }
    }
QUERY;

    public $wrapperQuery = <<<QUERY
mutation AssignLicenseMutation({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;

    protected $responseModel = User::class;
}
