<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Graphql\AbstractGraphql;
use Catalyst\Model\Input\AbstractInputModel;
use Catalyst\Requestable;

abstract class AbstractMutation extends AbstractGraphql
{

    public function create(AbstractInputModel $model): Requestable
    {
        return $this->createFromModel($model);
    }

    /**
     * @param  \Catalyst\Model\Input\AbstractInputModel[]  $models
     * @return \Catalyst\Requestable
     */
    public function createMany(array $models): Requestable
    {
        return $this->createFromModels($models);
    }
}
