<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Model\Response\User;

class CreateUser extends AbstractMutation
{

    protected $responseModel = User::class;

    public $contentQuery = <<<QUERY
    createUser({{contentVariables}}) {
        clientMutationId
        user {
            id
            email
            ssoId
            status
            languagePrograms {
                languageOfStudy
            }
        }
    }
QUERY;

    public $wrapperQuery = <<<QUERY
mutation CreateUserMutation({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;
}
