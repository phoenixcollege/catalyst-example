<?php

namespace Catalyst\Graphql\Mutation;

use Catalyst\Model\Response\User;

class RegisterUser extends AbstractMutation
{

    public $contentQuery = <<<QUERY
    registerUser({{contentVariables}}) {
        clientMutationId
        user {
            id
            email
            ssoId
            status
        }
    }
QUERY;

    public $wrapperQuery = <<<QUERY
mutation RegisterUserMutation({{wrapperVariables}}) {
    {{contentQuery}}
}
QUERY;

    protected $responseModel = User::class;
}
