<?php

namespace Catalyst\Graphql;

class Variable
{

    public $counter;

    public $key;

    public $type;

    public $value;

    public function __construct(string $key, $value, int $counter = null, string $type = 'String!')
    {
        $this->key = $key;
        $this->value = $value;
        $this->counter = $counter;
        $this->type = $type;
    }

    public function __toString()
    {
        return sprintf(
            '%s/%s[%s]: %s',
            $this->key,
            $this->getVariableKey(),
            $this->type,
            (is_array($this->value) ? json_encode($this->value) : (string) $this->value)
        );
    }

    public function getVariableKey($prefixDollarSign = true): string
    {
        $prefix = $prefixDollarSign ? '$' : null;
        return $prefix.$this->key.$this->counter;
    }
}
