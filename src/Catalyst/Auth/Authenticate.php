<?php

namespace Catalyst\Auth;

use Catalyst\Logging\Logger;
use Catalyst\Logging\LoggerTrait;
use Catalyst\Request\AbstractRequest;

class Authenticate
{

    use LoggerTrait;

    /**
     * @var AbstractRequest
     */
    public $request;

    protected $loggerKey = 'AUTH';

    public function __construct(AbstractRequest $request)
    {
        $this->request = $request;
    }

    public function authenticate(string $username, string $password): Token
    {
        $params = [
            'grant_type' => 'password',
            'client_id' => 'client.rsm2api',
            'username' => $username,
            'password' => $password,
        ];
        return $this->doAuthRequest($params);
    }

    public function refresh(Token $token): Token
    {
        $params = [
            'grant_type' => 'refresh_token',
            'client_id' => 'client.rsm2api',
            'refresh_token' => $token->refreshToken,
        ];
        return $this->doAuthRequest($params);
    }

    protected function doAuthRequest(array $params): Token
    {
        $this->log('Auth begins for type '.$params['grant_type']);
        $response = $this->request->request(
            http_build_query($params),
            ['Content-Type: application/x-www-form-urlencoded']
        );
        if ($response->hasErrors()) {
            throw new \Exception(implode(PHP_EOL, $response->getErrors()));
        }
        $responseArray = $response->toArray();
        $this->log('Auth complete');
        if (!$responseArray['access_token'] ?? null) {
            throw new \Exception("Invalid response to create token");
        }
        return new Token(
            $responseArray['access_token'],
            $responseArray['refresh_token'],
            $responseArray['expires_in']
        );
    }
}
