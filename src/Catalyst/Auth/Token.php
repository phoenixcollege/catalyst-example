<?php

namespace Catalyst\Auth;

use DateTime;

class Token
{

    /**
     * @var string
     */
    public $refreshToken;

    /**
     * @var string
     */
    public $token;

    /**
     * @var \DateTime
     */
    public $validUntil;

    public function __construct(
        $token,
        $refreshToken,
        $validUntil
    ) {
        $this->token = $token;
        $this->refreshToken = $refreshToken;
        $this->validUntil = $this->ensureDateTime($validUntil);
    }

    public function ensureDateTime($validUntil): DateTime
    {
        if ($validUntil instanceof DateTime) {
            return $validUntil;
        }
        return new DateTime('+'.$validUntil.' seconds');
    }

    public function isExpired(): bool
    {
        return (new DateTime()) >= $this->validUntil;
    }

    public function toHeader(): string
    {
        if ($this->isExpired()) {
            throw new \Exception('Token needs refresh.');
        }
        return 'Authorization: Bearer '.$this->token;
    }
}
